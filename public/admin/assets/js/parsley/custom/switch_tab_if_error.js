window.Parsley.on('field:error', function () {

    var $tabPane = this.$element.parents('.tab-pane');
    var invalidTabId = $tabPane.attr('id');

    if (!$tabPane.hasClass('active')) {
        $tabPane.parents('.tab-content')
            .find('.tab-pane')
            .each(function (index, tab) {
                var tabId = $(tab).attr('id'),
                    $li = $('a[href="#' + tabId + '"][data-toggle="tab"]').parent();

                if (tabId === invalidTabId) {
                    // activate the tab pane
                    $(tab).addClass('active');
                    // and the associated <li> element
                    $li.addClass('active');
                } else {
                    $(tab).removeClass('active');
                    $li.removeClass('active');
                }
            });
    }

});