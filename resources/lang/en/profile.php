<?php

return [
    'profile_settings'      => 'Profile Settings',
    'old_password'          => 'Old Password',
    'password'              => 'New password',
    'email'                 => 'Email',
    'password_confirmation' => 'Retype Password',
    'save'                  => 'Save',
    'cancel'                => 'Cancel',
    'dashboard'             => 'Dashboard'
];