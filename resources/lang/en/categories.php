<?php
return [

    'list'        => 'List',
    'categories'  => 'Categories',
    'dashboard'   => 'Dashboard',
    'id'          => 'ID',
    'name'        => 'Name',
    'description' => 'Description',
    'status'      => 'Status',
    'add_new'     => 'Add New',
    'actions'     => 'Action',
    'add'         => 'Add',
    'list_cat'    => 'List Categories',
    'save'        => 'Save',
    'cancel'      => 'Cancel',
    'active'      => 'Active',
    'inactive'    => 'Inactive',
    'saved'       => 'Category was successfully saved.',
    'error'       => 'An error occured:',
    'preview'     => 'Preview',
    'updated'     => 'Category was successfully updated.',
    'deleted'     => 'Category was successfully deleted.',
    'edit'        => 'Edit'

];