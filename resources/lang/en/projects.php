<?php
return [

    'list'          => 'List',
    'projects'      => 'Projects',
    'dashboard'     => 'Dashboard',
    'id'            => 'ID',
    'name'          => 'Name',
    'description'   => 'Description',
    'category'      => 'Category',
    'start_date'    => 'Start Date',
    'status'        => 'Status',
    'add_new'       => 'Add New',
    'actions'       => 'Action',
    'add'           => 'Add',
    'list_projects' => 'List Projects',
    'save'          => 'Save',
    'cancel'        => 'Cancel',
    'active'        => 'Active',
    'inactive'      => 'Inactive',
    'saved'         => 'Project was successfully saved.',
    'error'         => 'An error occured:',
    'preview'       => 'Preview',
    'updated'       => 'Project was successfully updated.',
    'deleted'       => 'Project was successfully deleted.',
    'edit'          => 'Edit'

];