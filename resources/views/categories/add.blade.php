@extends('admin.partials.layout')

@section('title',$title)

@section('meta-tags')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('css')
    <link href="{{ asset('/admin/assets/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
@stop
@section('topbar')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@lang('categories.categories')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">@lang('categories.dashboard')</a>
                </li>

                <li>
                    <a href="{{route('categories.index')}}">@lang('categories.categories')</a>
                </li>
                <li class="active">
                    <strong>@lang('categories.add_new')</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@stop

@section('content')
    @include('admin.partials.inline_messages')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('categories.add_new')</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('categories.store')}}" data-parsley-validate>
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('categories.name') *</label>

                                <div class="col-sm-10"><input type="text" placeholder="@lang('categories.name')"
                                                              class="form-control" name="name" value="{{old('name')}}" data-parsley-required></div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('categories.description') *</label>

                                <div class="col-sm-10">
                                    <textarea type="text"  name="description" class="form-control" rows="3"  required>{{old('description')}} </textarea>

                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('categories.status') *</label>

                                <div class="col-sm-10">
                                    <div class="i-checks"><label> <input type="radio" checked="" value="active" name="status"> <i></i> @lang('categories.active') </label></div>
                                    <div class="i-checks"><label> <input type="radio"  value="inactive" name="status"> <i></i> @lang('categories.inactive') </label></div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="pull-right" style="padding: 10px 15px;">
                                <button type="submit" class="btn btn-w-m btn-primary">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>@lang('categories.save')
                                </button>
                                <a href="{{route('categories.index')}}">
                                    <button type="button" class="btn btn-w-m btn-danger">
                                        <span class="glyphicon glyphicon-ban-circle"></span> @lang('categories.cancel')
                                    </button>
                                </a>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_theme_javascript')
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection

@section('js')
    <!-- ICheck -->
    <script src="{{ asset('/admin/assets/js/plugins/iCheck/icheck.min.js') }}"></script>
@endsection


