@extends('admin.partials.layout')

@section('title',$title)

@section('meta-tags')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('css')
    <link href="{{ asset('/admin/assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop
@section('topbar')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>@lang('categories.categories')</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">@lang('categories.dashboard')</a>
            </li>

            <li class="active">
                <strong>@lang('categories.categories')</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
    @stop

@section('content')
    @include('admin.partials.messages')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <a href="{{route('categories.create')}}" style="color: white;"><button type="button" class="btn btn-w-m btn-primary pull-right" style="margin: 7px 10px;">@lang('categories.add_new')</button></a>

                <div class="ibox-title">
                    <h5>@lang('categories.list_cat')</h5>


                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table id="categories-datatable" class="table table-striped table-bordered table-hover" >
                            <thead>
                            <tr>
                                <th>@lang('categories.id')</th>
                                <th>@lang('categories.name')</th>
                                <th>@lang('categories.description')</th>
                                <th>@lang('categories.status')</th>
                                <th>@lang('categories.actions')</th>
                            </tr>
                            </thead>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_theme_javascript')
<script>
    $(document).ready(function () {
        $('#categories-datatable').DataTable({
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [-1]
            }],
            aaSorting: [[0, 'DESC']],
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
            serverSide: true,
            processing: true,
            destroy: true,
            render: true,
            columns: [
                {data: 'id', "defaultContent": "<i>Not set</i>", "bVisible": true},
                {data: 'name', "defaultContent": "<i>Not set</i>"},
                {data: 'description', "defaultContent": "<i>Not set</i>"},
                {data: 'status', "searchable": false, "orderable": false},
                {data: 'action', "searchable": false, "orderable": false}
            ],
            order: [[ 2, "ASC" ]],
            ajax: '{{route('data')}}'

        });

    });
    // Delete category
    function deleteItem(id) {
        if (window.confirm("Are you sure?")) {
            var delete_url = "{!! route('categories.destroy', ":id") !!}";
            delete_url = delete_url.replace(':id', id);

            $.ajax({
                url: delete_url,
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    window.location.reload();
                },
                error: function (result) {
                    window.location.reload();
                }
            });
        }
    }
</script>
@endsection

@section('js')
    <script src="{{ asset('/admin/assets/js/plugins/dataTables/datatables.min.js') }}"></script>
@endsection


