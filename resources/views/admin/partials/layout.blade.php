<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>My Team | @yield('title')</title>

    <link rel="icon" type="image/png" href="{{ asset('img/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('img/favicon-16x16.png') }}" sizes="16x16" />
    @yield('meta-tags')

    @include('admin.partials.css.global_css')

    @yield('css')

    @include('admin.partials.js.main_js')
</head>

<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
            @include('admin.partials.sidebar_left')
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">

                @include('admin.partials.navbar')

        </div>
        @yield('topbar')

        <div class="wrapper wrapper-content">

                 @yield('content')
        </div>

                 @include('admin.partials.footer')

    </div>
        @include('admin.partials.sidebar_right')
</div>

<div class="loading">Loading&#8230;</div>


<!-- Custom and plugin javascript -->
@include('admin.partials.js.custom_js')

<!-- jQuery UI -->
@include('admin.partials.js.jquery_bottom_page')

@yield('custom_theme_javascript')

@yield('js')
</body>
</html>
