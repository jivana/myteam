@if(Session::has('error'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissible admin_message" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-times" aria-hidden="true"></i> <strong>{{ Session::get('error') }}</strong>
            </div>
        </div>
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li style="list-style-type: none;">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('info'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-primary alert-dismissible admin_message" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-info-circle" aria-hidden="true"></i> <strong>{{ Session::get('info') }}</strong>
            </div>
        </div>
    </div>
@endif

@if(Session::has('success'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible admin_message" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-check" aria-hidden="true"></i> <strong>{{ Session::get('success') }}</strong>
            </div>
        </div>
    </div>
@endif