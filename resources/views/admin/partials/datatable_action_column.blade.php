@isset($preview)
    <a class="btn btn-xs btn-success" href="{{ $preview }}" @isset($target) target="{{ $target }}" @endisset><i class="glyphicon glyphicon-eye-open"></i> Preview</a>
@endisset
@isset($edit)
    <a class="btn btn-xs btn-warning" href="{{ $edit }}" @isset($target) target="{{ $target }}" @endisset><i class="glyphicon glyphicon-edit"></i> Edit</a>
@endisset
@isset($delete)
    <a class="btn btn-xs btn-danger" onClick='deleteItem({{ $delete }}, this)' @isset($target) target="{{ $target }}" @endisset><i class="glyphicon glyphicon-remove"></i> Delete</a>
@endisset