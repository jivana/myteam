<div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
        <li class="nav-header">
            <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{ asset('/admin/assets/img/user_default.png') }}" />
                             </span>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name}}</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="{{route('profile-settings.index')}}">Profile</a></li>

                    <li class="divider"></li>
                    <li><a href="{{route('logout')}}">Logout</a></li>
                </ul>
            </div>
            <div class="logo-element">
                MT
            </div>
        </li>
        <li class="@if(Request::segment(1) == 'projects') active @endif">
            <a href="{{route('projects.index')}}"><i class="fa fa-bullhorn"></i> <span class="nav-label">Project Management</span></a>
        </li>
        <li class="@if(Request::segment(1) == 'categories') active @endif">
            <a href="{{route('categories.index')}}"><i class="fa fa-bullhorn"></i> <span class="nav-label">Categories</span></a>
        </li>

    </ul>
</div>