<!-- Mainly scripts -->
<script src="{{ asset('/admin/assets/js/jquery-3.1.1.min.js') }}"></script>

<script src="{{ asset('/admin/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/admin/assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('/admin/assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Sweet alert -->
<script src="{{ asset('/admin/assets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<!-- Parsley -->
<script src="{{ asset('/admin/assets/js/parsley/parsley.js') }}"></script>
<script src="{{ asset('/admin/assets/js/parsley/extra/validator/comparison.js') }}"></script>