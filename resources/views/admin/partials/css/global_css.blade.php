<link href="{{ asset('/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<link href="{{ asset('/admin/assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/assets/css/style.css') }}" rel="stylesheet">


<!-- Sweet Alert -->
<link href="{{ asset('/admin/assets/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/assets/css/animate.css') }}" rel="stylesheet">

<link href="{{ asset('/admin/assets/css/parsley/parsley.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/assets/css/custom.css') }}" rel="stylesheet">

<!-- Favicon -->
<link rel="shortcut icon" href="{{asset('admin/assets/img/favicon.png')}}">