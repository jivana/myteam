@if(Session::has('error'))
    <script>
        $(document).ready(function(){
            swal({
                title: "Ops!",
                text: "{{ Session::get('error') }}",
                type: "error"
            });
        });
    </script>
@endif

@if (count($errors) > 0)
    <script>
        $(document).ready(function(){
            swal({
                title: "Ops!",
                html: '{!! implode(" <br>",$errors->all()) !!}',
                type: "error"
            });
        });
    </script>
@endif

@if(Session::has('info'))
    <script>
        $(document).ready(function(){
            swal({
                title: "Ops!",
                html: '{{ Session::get('info') }}',
                type: "info"
            });
        });
    </script>
@endif

@if(Session::has('success'))
    <script>
        $(document).ready(function(){
            swal({
                title: "Good job!",
                text: "{{ Session::get('success') }}",
                type: "success",
                timer: 2000
            });
        });
    </script>
@endif