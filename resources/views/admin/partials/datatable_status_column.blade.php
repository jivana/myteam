<div class="buttons-justify">
    @if($active)
        <span class="badge badge-primary badge-width">Active</span>

    @else
        <span class="badge badge-danger badge-width">Inactive</span>
    @endif
</div>