<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>My Team | Register</title>

    <link href="{{ asset('/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/admin/assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/css/parsley/parsley.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{ asset('/admin/assets/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('admin/assets/img/favicon.png')}}">


</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>

            <img class="img-responsive" src="{{ asset('/admin/assets/img/logo.png') }}" style="margin-left: 17%;">

        </div>
        <h3>Register</h3>

        <form class="m-t" role="form" method="POST" action="{{route('postRegister')}}" data-parsley-validate>
            {!! csrf_field() !!}
            @include('admin.partials.inline_messages')

            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name')}}" data-parsley-required>
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" value="{{old('email')}}" data-parsley-required>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" data-parsley-required>
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Retype Password" data-parsley-required>
            </div>
            <div class="form-group">
                <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> Agree the terms and policy </label></div>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

            <p class="text-muted text-center"><small>Already have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{route('login')}}">Login</a>
        </form>
        <p class="m-t"> <small>Ivana Jakimovska &copy; {{date('Y')}}</small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('/admin/assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('/admin/assets/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/admin/assets/js/plugins/iCheck/icheck.min.js') }}"></script>
<!-- Parsley -->
<script src="{{ asset('/admin/assets/js/parsley/parsley.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('/admin/assets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script>

    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });

</script>
</body>

</html>
