<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>MyTeam | Login</title>

    <link href="{{ asset('/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/admin/assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/css/style.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('admin/assets/img/favicon.png')}}">

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <img class="img-responsive" src="{{ asset('/admin/assets/img/logo.png') }}" style="margin-left: 17%;">

        </div>

        <form class="m-t" method="post" role="form" action="{{route('postLogin')}}">
            {{csrf_field()}}
            @include('admin.partials.inline_messages')
            <div class="form-group">
                <input type="email" class="form-control"  name="email" placeholder="Username" required="">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{route('register')}}">Create an account</a>
        </form>
        <p class="m-t"> <small>Ivana Jakimovska &copy; {{date('Y')}}</small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('/admin/assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('/admin/assets/js/bootstrap.min.js') }}"></script>

</body>

</html>
