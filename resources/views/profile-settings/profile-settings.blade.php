@extends('admin.partials.layout')

@section('title',$title)

@section('meta-tags')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('css')
    <link href="{{ asset('/admin/assets/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
@stop
@section('topbar')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@lang('profile.profile_settings')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">@lang('profile.dashboard')</a>
                </li>

                <li class="active">
                    <a href="{{route('profile-settings.index')}}"><strong>@lang('profile.profile_settings')</strong></a>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@stop

@section('content')
    @include('admin.partials.messages')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('profile.profile_settings')</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" method="POST" action="{{route('profile-settings.update', Auth::user()->id)}}" data-parsley-validate>
                        {!! csrf_field() !!}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('profile.email') *</label>

                                <div class="col-sm-10"><input type="email" class="form-control"  name="email" placeholder="Email"  @if(isset(Auth::user()->email)) value="{{ Auth::user()->email }}" @endif readonly>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('profile.old_password') *</label>

                                <div class="col-sm-10"> <input type="password" name="old_password" class="form-control" placeholder="@lang('profile.old_password')" required="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('profile.password') *</label>

                                <div class="col-sm-10"> <input type="password" name="password" class="form-control" placeholder="@lang('profile.password')" required="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('profile.password_confirmation') *</label>

                                <div class="col-sm-10"> <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('profile.password_confirmation')" required="">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="pull-right" style="padding: 10px 15px;">
                                <button type="submit" class="btn btn-w-m btn-primary">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>@lang('profile.save')
                                </button>
                                <a href="{{route('profile-settings.index')}}">
                                    <button type="button" class="btn btn-w-m btn-danger">
                                        <span class="glyphicon glyphicon-ban-circle"></span> @lang('profile.cancel')
                                    </button>
                                </a>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_theme_javascript')
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection

@section('js')
    <!-- ICheck -->
    <script src="{{ asset('/admin/assets/js/plugins/iCheck/icheck.min.js') }}"></script>
@endsection


