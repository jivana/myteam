@extends('admin.partials.layout')

@section('title',$title)

@section('meta-tags')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('css')
    <link href="{{ asset('/admin/assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@stop
@section('topbar')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@lang('projects.projects')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">@lang('projects.dashboard')</a>
                </li>

                <li class="active">
                    <strong>@lang('projects.projects')</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@stop

@section('content')
    @include('admin.partials.messages')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <a href="{{route('projects.create')}}" style="color: white;"><button type="button" class="btn btn-w-m btn-primary pull-right" style="margin: 7px 10px;">@lang('projects.add_new')</button></a>

                <div class="ibox-title">
                    <h5>@lang('projects.list_projects')</h5>


                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table id="projects-datatable" class="table table-striped table-bordered table-hover" >
                            <thead>
                            <tr>
                                <th>@lang('projects.id')</th>
                                <th>@lang('projects.name')</th>
                                <th>@lang('projects.description')</th>
                                <th>@lang('projects.category')</th>
                                <th>@lang('projects.start_date')</th>
                                <th>@lang('projects.status')</th>
                                <th>@lang('projects.actions')</th>
                            </tr>
                            </thead>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_theme_javascript')
    <script>
        $(document).ready(function () {
            $('#projects-datatable').DataTable({
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [-1]
                }],
                aaSorting: [[0, 'DESC']],
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "",
                        "sNext": ""
                    }
                },
                "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                serverSide: true,
                processing: true,
                destroy: true,
                render: true,
                columns: [
                    {data: 'id', "defaultContent": "<i>Not set</i>", "bVisible": true},
                    {data: 'name', "defaultContent": "<i>Not set</i>"},
                    {data: 'description', "defaultContent": "<i>Not set</i>"},
                    {data: 'categories[, ].name', "defaultContent": "<i>Not set</i>"},
                    {data: 'start_date', "defaultContent": "<i>Not set</i>"},
                    {data: 'status', "searchable": false, "orderable": false},
                    {data: 'action', "searchable": false, "orderable": false}
                ],
                order: [[ 2, "ASC" ]],
                ajax: '{{route('data-projects')}}'

            });

        });

        // Delete project
        function deleteItem(id) {
            if (window.confirm("Are you sure?")) {
                var delete_url = "{!! route('projects.destroy', ":id") !!}";
                delete_url = delete_url.replace(':id', id);

                $.ajax({
                    url: delete_url,
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (result) {
                        window.location.reload();
                    },
                    error: function (result) {
                        window.location.reload();
                    }
                });
            }
        }
    </script>
@endsection

@section('js')
    <script src="{{ asset('/admin/assets/js/plugins/dataTables/datatables.min.js') }}"></script>
@endsection


