@extends('admin.partials.layout')

@section('title',$title)

@section('meta-tags')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('css')
    <link href="{{ asset('/admin/assets/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/admin/assets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
@stop
@section('topbar')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>@lang('projects.projects')</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">@lang('projects.dashboard')</a>
                </li>

                <li>
                    <a href="{{route('projects.index')}}">@lang('projects.projects')</a>
                </li>
                <li class="active">
                    <strong>@lang('projects.add_new')</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@stop

@section('content')
    @include('admin.partials.inline_messages')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('projects.add_new')</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" role="form" id="form" method="POST" action="{{route('projects.store')}}" data-parsley-validate>
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('projects.name') *</label>

                                <div class="col-sm-10"><input type="text"
                                                              class="form-control" name="name" value="{{old('name')}}" data-parsley-required></div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('projects.description') *</label>

                                <div class="col-sm-10">
                                    <textarea type="text"  name="description" class="form-control" rows="3"  required>{{old('description')}} </textarea>

                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label text-right">@lang('projects.category') *</label>

                                <div class="col-sm-10">
                                    <select class="form-control m-b" name="categories[]" multiple data-parsley-required>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group " >
                            <div class="row">
                                <label class="col-sm-2 control-label text-right">@lang('projects.start_date') *<br/></label>

                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control data-picker"   name="start_date" value="{{ old('start_date')}}" data-parsley-mindate="<?php echo date('Y-m-d'); ?>" data-parsley-required>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">@lang('projects.status') *</label>

                                <div class="col-sm-10">
                                    <div class="i-checks"><label> <input type="radio" checked="" value="active" name="status"> <i></i> @lang('projects.active') </label></div>
                                    <div class="i-checks"><label> <input type="radio"  value="inactive" name="status"> <i></i> @lang('projects.inactive') </label></div>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <div class="pull-right" style="padding: 10px 15px;">
                                <button type="submit" class="btn btn-w-m btn-primary">
                                    <span class="glyphicon glyphicon-floppy-disk"></span>@lang('projects.save')
                                </button>
                                <a href="{{route('projects.index')}}">
                                    <button type="button" class="btn btn-w-m btn-danger">
                                        <span class="glyphicon glyphicon-ban-circle"></span> @lang('projects.cancel')
                                    </button>
                                </a>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_theme_javascript')
    <script>
        //Validation for start date
        window.ParsleyValidator
            .addValidator('mindate', function (value, requirement) {
                // is valid date?
                var timestamp = Date.parse(value),
                    minTs = Date.parse(requirement);

                return isNaN(timestamp) ? false : timestamp > minTs;
            }, 32)
            .addMessage('en', 'mindate', 'This date should be greater than %s');

        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('select[name="categories[]"]').select2();


        });
        $('body').on('focus',".data-picker", function(){
            $(this).datepicker({
                format: 'yyyy-mm-dd',
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                onClose: function () {
                    $(this).parsley().validate();
                }
            });
        });


    </script>
@endsection

@section('js')
    <!-- ICheck -->
    <script src="{{ asset('/admin/assets/js/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('/admin/assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <!-- Data picker -->
    <script src="{{ asset('/admin/assets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
@endsection


