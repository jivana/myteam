##### MY Team #####
A web application for everyone who has ideas but do not have a team. 
They can publish their project idea on 'My team' platform where in the description of the project 
itself would indicate what kind of people they need (eg PHP developer,
marketer, sales person ...) 
On the other hand, those who are enthusiastic and want to work something innovative and with people
who may not even know they can join their team in order to
help develop the idea or become part of the team.