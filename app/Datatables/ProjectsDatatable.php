<?php

namespace App\Datatables;



class ProjectsDatatable extends AbstractDatatable
{

    public function editColumns()
    {
        $this->datatable->addColumn('status', function ($project) {
            $active= '';
            if($project->status == 'active')
            {
                $active = $project->status;
            }
            $status = (string) view('admin.partials.datatable_status_column', [
                'active' => $active
            ]);

            return $status;
        });
        $this->datatable->addColumn('action', function ($project) {

            $action = (string) view('admin.partials.datatable_action_column', [
                'preview' => route('projects.show', $project->id),
                'edit'    => route('projects.edit', $project->id),
                'delete'  => $project->id
            ]);

            return $action;
        });

        return $this->datatable;

    }

}