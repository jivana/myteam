<?php

namespace App\Datatables;



class CategoriesDatatable extends AbstractDatatable
{

    public function editColumns()
    {
        $this->datatable->addColumn('status', function ($category) {
            $active= '';
            if($category->status == 'active')
            {
                $active = $category->status;
            }
            $status = (string) view('admin.partials.datatable_status_column', [
                'active' => $active
            ]);

            return $status;
        });
        $this->datatable->addColumn('action', function ($category) {

            $action = (string) view('admin.partials.datatable_action_column', [
                'preview' => route('categories.show', $category->id),
                'edit'    => route('categories.edit', $category->id),
                'delete'  => $category->id
            ]);

            return $action;
        });

        return $this->datatable;

    }

}