<?php

namespace App\Datatables;


use Yajra\DataTables\DataTables;

abstract class AbstractDatatable
{

    protected $datatable;

    public function of(DataTables $dataTable, $resource)
    {
        $this->datatable=$dataTable->of($resource);
        $this->editColumns();

        return $this->datatable->escapeColumns([ 'action' ])->make(true);
    }

    abstract protected function editColumns();
}