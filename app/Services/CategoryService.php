<?php


namespace App\Services;

use App\Http\Requests\Categories\AddCategoryFormRequest;
use App\Http\Requests\Categories\UpdateCategoryFormRequest;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\DatabaseTransactionRepository;

class CategoryService
{

    private $databaseTransaction;
    private $categoryRepository;


    /**
     * CategoryService constructor.
     *
     * @param $databaseTransaction
     * @param $categoryRepository
     */
    public function __construct(DatabaseTransactionRepository $databaseTransaction, CategoryRepositoryEloquent $categoryRepository)
    {
        $this->databaseTransaction = $databaseTransaction;
        $this->categoryRepository = $categoryRepository;
    }

    public function save(AddCategoryFormRequest $request){
        $this->databaseTransaction->beginTransaction();

        try{

            $dataArray= $request->only([
                'name',
                'description',
                'status'
            ]);

            $this->categoryRepository->create($dataArray);
            $this->databaseTransaction->commit();
        }
        catch (\Exception $e){

            $this->databaseTransaction->rollback();
            throw $e;
        }
    }

    public function getAllForDatatable(){
        return $this->categoryRepository->all();
    }

    public function get($id)
    {
        return $this->categoryRepository->find($id);
    }

    public function update(UpdateCategoryFormRequest $request, $id){

        $this->databaseTransaction->beginTransaction();

        try{
            $dataArray= $request->only([
                'name',
                'description',
                'status'
            ]);

            $this->categoryRepository->update($dataArray,$id);
            $this->databaseTransaction->commit();
        }
        catch (\Exception $e){
            $this->databaseTransaction->rollback();
            throw $e;
        }
    }


    public function delete($id){
        $this->databaseTransaction->beginTransaction();
        try{

            $this->categoryRepository->delete($id);
            $this->databaseTransaction->commit();
        }
        catch (\Exception $e){
            $this->databaseTransaction->rollback();
            throw $e;
        }
    }


    public function getAll()
    {
        return $this->categoryRepository->all();
    }
}