<?php


namespace App\Services;


use App\Http\Requests\Auth\LoginFormRequest;
use App\Repositories\DatabaseTransactionRepository;
use App\Repositories\UserRepositoryEloquent;

class LoginService
{

    private $databaseTransaction;
    private $userRepository;


    /**
     * LoginService constructor.
     *
     * @param $databaseTransaction
     */
    public function __construct(DatabaseTransactionRepository $databaseTransaction, UserRepositoryEloquent $userRepository)
    {
        $this->databaseTransaction = $databaseTransaction;
        $this->userRepository=$userRepository;

    }


    public function login(LoginFormRequest $request){

        $this->databaseTransaction->beginTransaction();
        try{

            $dataArray = $request->only([ 'email', 'password' ]);
            $this->userRepository->loginUser($dataArray);
            $this->databaseTransaction->commit();
        }
        catch (\Exception $e)
        {
            $this->databaseTransaction->rollback();
            throw $e;
        }
    }
}