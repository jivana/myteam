<?php


namespace App\Services;

use App\Http\Requests\Projects\AddProjectFormRequest;
use App\Http\Requests\Projects\UpdateProjectFormRequest;
use App\Repositories\DatabaseTransactionRepository;
use App\Repositories\ProjectRepositoryEloquent;

class ProjectService
{

    private $databaseTransaction;
    private $projectRepository;


    /**
     * CategoryService constructor.
     *
     * @param $databaseTransaction
     * @param $projectRepository
     */
    public function __construct(DatabaseTransactionRepository $databaseTransaction, ProjectRepositoryEloquent $projectRepository)
    {
        $this->databaseTransaction = $databaseTransaction;
        $this->projectRepository = $projectRepository;
    }

    public function save(AddProjectFormRequest $request){
        $this->databaseTransaction->beginTransaction();

        try{

            $dataArray= $request->only([
                'name',
                'description',
                'status',
                'start_date'
            ]);

           $project= $this->projectRepository->create($dataArray);
           $categoryArray= $request->only('categories');

           $project->categories()->sync($categoryArray['categories']);


            $this->databaseTransaction->commit();
        }
        catch (\Exception $e){

            $this->databaseTransaction->rollback();
            throw $e;
        }
    }


    public function getAllForDatatable()
    {
        return $this->projectRepository->getAllForDatatable();
    }


    public function get($id)
    {
        return $this->projectRepository->find($id);
    }


    public function delete($id)
    {
        $this->databaseTransaction->beginTransaction();
        try {

            $this->projectRepository->delete($id);
            $this->databaseTransaction->commit();
        } catch (\Exception $e) {
            $this->databaseTransaction->rollback();
            throw $e;
        }
    }

    public function update(UpdateProjectFormRequest $request , $id){

        $this->databaseTransaction->beginTransaction();
        try{
            $dataArray= $request->only([
                'name',
                'description',
                'status',
                'start_date'
            ]);

           $project= $this->projectRepository->update($dataArray,$id);

            $catArray= $request->only('categories');
            $project->categories()->sync($catArray['categories']);
            $this->databaseTransaction->commit();
        }
        catch (\Exception $e){
            $this->databaseTransaction->rollback();
            throw  $e;
        }
    }
}