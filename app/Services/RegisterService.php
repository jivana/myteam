<?php


namespace App\Services;


use App\Http\Requests\Auth\RegisterFormRequest;
use App\Repositories\DatabaseTransactionRepository;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Support\Facades\Auth;

class RegisterService
{

    private $databaseTransaction;
    private $userRepository;


    /**
     * LoginService constructor.
     *
     * @param $databaseTransaction
     */
    public function __construct(DatabaseTransactionRepository $databaseTransaction, UserRepositoryEloquent $userRepository)
    {
        $this->databaseTransaction = $databaseTransaction;
        $this->userRepository=$userRepository;

    }


    public function register(RegisterFormRequest $request)
    {
        $this->databaseTransaction->beginTransaction();
        try {
            $dataArray = $request->only([
                'name',
                'email',
                'password'
            ]);
            $user = $this->userRepository->create($dataArray);
            $this->databaseTransaction->commit();

            Auth::loginUsingId($user->id);
        } catch (\Exception $e) {
            $this->databaseTransaction->rollback();
            throw $e;
        }
    }


}