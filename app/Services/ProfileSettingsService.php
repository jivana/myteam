<?php


namespace App\Services;


use App\Http\Requests\Profile\ProfileSettingsFormRequest;
use App\Repositories\DatabaseTransactionRepository;
use App\Repositories\ProfileSettingsRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;

class ProfileSettingsService
{

    private $profileSettingsRepository;

    private $databaseTransactionRepository;

    private $userRepositoryEloquent;


    /**
     * ProfileSettingsService constructor.
     *
     * @param $profileSettingsRepository
     */
    public function __construct(
        ProfileSettingsRepositoryEloquent $profileSettingsRepository,
        DatabaseTransactionRepository $databaseTransactionRepository,
    UserRepositoryEloquent $userRepositoryEloquent
    ) {
        $this->profileSettingsRepository = $profileSettingsRepository;
        $this->databaseTransactionRepository = $databaseTransactionRepository;
        $this->userRepositoryEloquent=$userRepositoryEloquent;
    }


    public function updateUser(ProfileSettingsFormRequest $request, $id)
    {

        $this->databaseTransactionRepository->beginTransaction();

        try {
            $data = $request->only([ 'password' ]);
            $this->userRepositoryEloquent->update($data, $id);
            $this->databaseTransactionRepository->commit();
        } catch (\Exception $e) {
            $this->databaseTransactionRepository->rollback();
            throw $e;
        }

    }
}