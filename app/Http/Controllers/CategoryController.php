<?php

namespace App\Http\Controllers;

use App\Datatables\CategoriesDatatable;
use App\Http\Requests\Categories\AddCategoryFormRequest;

use App\Http\Requests\Categories\UpdateCategoryFormRequest;
use App\Services\CategoryService;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{

    private $categoryService;

    private $categoriesDatatable;


    /**
     * CategoryController constructor.
     *
     * @param $categoryService
     */
    public function __construct(CategoryService $categoryService, CategoriesDatatable $categoriesDatatable)
    {
        $this->categoryService = $categoryService;
        $this->categoriesDatatable = $categoriesDatatable;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('categories.list', [
            'title' => Lang::get('categories.list')
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.add', [
            'title' => Lang::get('categories.add')
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AddCategoryFormRequest $request)
    {

        try {
            $this->categoryService->save($request);
            Session::flash('success', Lang::get('categories.saved'));

            return redirect()->route('categories.index');
        } catch (\Exception $e) {
            Session::flash('error', Lang::get('categories.error').$e->getMessage());

            return redirect()->back();
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->categoryService->get($id);

        return view('categories.preview', [
            'title'    => 'Preview',
            'category' => $category
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryService->get($id);

        return view('categories.edit', [
            'title'    => 'Edit',
            'category' => $category
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryFormRequest $request, $id)
    {
        try {
            $this->categoryService->update($request,$id);
            Session::flash('success', Lang::get('categories.updated'));

            return redirect()->route('categories.index');
        } catch (\Exception $e) {
            Session::flash('error', Lang::get('categories.error').$e->getMessage());

            return redirect()->back();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->categoryService->delete($id);
            Session::flash('success', Lang::get('categories.deleted'));

            return redirect()->route('categories.index');
        } catch (\Exception $e) {
            Session::flash('error', Lang::get('categories.error').$e->getMessage());

            return redirect()->back();
        }
    }


    public function getData(DataTables $dataTable)
    {

        $category = $this->categoryService->getAllForDatatable();

        return $this->categoriesDatatable->of($dataTable, $category);
    }
}
