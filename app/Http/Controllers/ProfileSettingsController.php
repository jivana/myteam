<?php

namespace App\Http\Controllers;

use App\Http\Requests\Profile\ProfileSettingsFormRequest;

use App\Services\ProfileSettingsService;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class ProfileSettingsController extends Controller
{
    //

    private $profileSettingsService;


    /**
     * ProfileSettingsController constructor.
     *
     * @param $profileSettingsService
     */
    public function __construct(ProfileSettingsService $profileSettingsService)
    {
        $this->profileSettingsService = $profileSettingsService;
    }


    public function index(){
        return view('profile-settings.profile-settings',[
            'title' => Lang::get('profile.profile_settings')
        ]);
    }


    public function update(ProfileSettingsFormRequest $request,$id){

        try {
            $this->profileSettingsService->updateUser($request,$id);
        } catch (\Exception $e) {
            Session::flash('error', 'An error occured: '.$e->getMesasge());

            return redirect()->back();
        }

        Session::flash('success', 'Your information was updated successfully!');

         return redirect()->back();
    }


}
