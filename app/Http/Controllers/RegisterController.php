<?php

namespace App\Http\Controllers;


use App\Http\Requests\Auth\RegisterFormRequest;
use App\Services\RegisterService;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{

    private $registerService;


    /**
     * RegisterController constructor.
     *
     * @param $registerService
     */
    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
    }


    public function getRegister(){
        return view('auth.register');
    }

    public function postRegister(RegisterFormRequest $request){

        try {
            $this->registerService->register($request);


            return redirect('/');
        } catch (\Exception $e) {

            return redirect()->back()->withErrors([ 'error' => 'Something went wrong!' ])->withInput();
        }
    }
}
