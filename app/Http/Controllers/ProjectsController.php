<?php

namespace App\Http\Controllers;

use App\Datatables\ProjectsDatatable;
use App\Http\Requests\Projects\AddProjectFormRequest;
use App\Http\Requests\Projects\UpdateProjectFormRequest;
use App\Services\CategoryService;
use App\Services\ProjectService;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class ProjectsController extends Controller
{
    private $categoryService;
    private $projectService;
    private $projectsDatatable;

    /**
     * ProjectsController constructor.
     *
     * @param $categoryService
     */
    public function __construct(CategoryService $categoryService, ProjectService $projectService, ProjectsDatatable $projectsDatatable)
    {
        $this->categoryService = $categoryService;
        $this->projectService= $projectService;
        $this->projectsDatatable=$projectsDatatable;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projects.list',[
            'title' => 'List Projects'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories= $this->categoryService->getAll();
        return view('projects.add', [
            'title' => Lang::get('projects.add'),
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddProjectFormRequest $request)
    {
        try {
            $this->projectService->save($request);
            Session::flash('success', Lang::get('projects.saved'));

            return redirect()->route('projects.index');
        } catch (\Exception $e) {
            Session::flash('error', Lang::get('projects.error').$e->getMessage());

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project= $this->projectService->get($id);
        return view('projects.preview', [
            'title' => Lang::get('projects.preview'),
            'project' => $project
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project= $this->projectService->get($id);
        $categories= $this->categoryService->getAll();
        return view('projects.edit', [
            'title' => Lang::get('projects.edit'),
            'project' => $project,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProjectFormRequest $request, $id)
    {
        try {
            $this->projectService->update($request,$id);
            Session::flash('success', Lang::get('projects.updated'));
            return redirect()->route('projects.index');
        } catch (\Exception $e) {

            Session::flash('error', Lang::get('projects.error').$e->getMessage());

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->projectService->delete($id);
            Session::flash('success', Lang::get('projects.deleted'));

            return redirect()->route('projects.index');
        } catch (\Exception $e) {
            Session::flash('error', Lang::get('projects.error').$e->getMessage());

            return redirect()->back();
        }
    }

    public function getData(DataTables $dataTable){

        $project = $this->projectService->getAllForDatatable();

        return $this->projectsDatatable->of($dataTable, $project);
    }
}
