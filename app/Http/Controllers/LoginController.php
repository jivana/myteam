<?php

namespace App\Http\Controllers;


use App\Http\Requests\Auth\LoginFormRequest;
use App\Services\LoginService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //

    private $loginService;


    /**
     * LoginController constructor.
     *
     * @param $loginService
     */
    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    public function getLogin(){
        return view('auth.login');
    }

    public function postLogin(LoginFormRequest $request){

        try {
            $this->loginService->login($request);
            return redirect('/');
        } catch (\Exception $e) {

            return redirect()->back()->withErrors([ 'error' => 'Invalid credentials!' ])->withInput();
        }

    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->flush();

        return redirect()->route('login');
    }
}
