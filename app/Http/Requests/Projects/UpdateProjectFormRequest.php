<?php

namespace App\Http\Requests\Projects;

use App\Entities\Project;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProjectFormRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(2);

        return [
            'name'         => 'required|string|unique:projects,name,'.$id.',id',
            'description'  => 'required|string',
            'categories.*' => 'required',
            'start_date'   => 'required|date',
            'status'       => 'required'
        ];
    }

}
