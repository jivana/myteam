<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileSettingsFormRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'nullable|old_password:'.Auth::user()->password,
            'password'     => 'nullable|confirmed|different:old_password',
        ];
    }


    public function messages()
    {
        return [
            'old_password' => 'The old password is not correct!'
        ];
    }
}
