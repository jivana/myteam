<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProfileSettingsRepository;
use App\Entities\ProfileSettings;
use App\Validators\ProfileSettingsValidator;

/**
 * Class ProfileSettingsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProfileSettingsRepositoryEloquent extends BaseRepository implements ProfileSettingsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProfileSettings::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
