<?php

namespace App\Repositories;


class DatabaseTransactionRepository
{


    public function beginTransaction()
    {
        app('db')->beginTransaction();
    }

    public function commit()
    {
        app('db')->commit();
    }

    public function rollback()
    {
        app('db')->rollback();
    }
    
}
