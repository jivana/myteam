<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([ 'middleware' => 'checkLogin' ], function () {
    //Login
    Route::get('login', LoginController::class.'@getLogin')->name('login');
    Route::post('login', LoginController::class.'@postLogin')->name('postLogin');
});

//Routes that needs a logged in user
Route::group([ 'middleware' => 'auth' ], function () {
    Route::get('/', function () {
        return view('blank');
    });
    //Projects
    Route::resource('projects', ProjectsController::class);
    Route::get('projects-data', ProjectsController::class.'@getData')->name('data-projects');
    //Categories
    Route::resource('categories', CategoryController::class);
    Route::get('data', CategoryController::class.'@getData')->name('data');

    //Login
    Route::get('logout', 'LoginController@logout')->name('logout');

    //Profile Settings
    Route::resource('profile-settings', ProfileSettingsController::class);

});

//Register
Route::get('register', RegisterController::class.'@getRegister')->name('register');
Route::post('register', RegisterController::class.'@postRegister')->name('postRegister');